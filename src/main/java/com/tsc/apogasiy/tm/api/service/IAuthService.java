package main.java.com.tsc.apogasiy.tm.api.service;

import main.java.com.tsc.apogasiy.tm.enumerated.Role;

public interface IAuthService {

    String getCurrentUserId();

    void setCurrentUserId(String userId);

    boolean isAuth();

    boolean isAdmin();

    void login(String login, String password);

    void logout();

    void checkRoles(Role... roles);

}
