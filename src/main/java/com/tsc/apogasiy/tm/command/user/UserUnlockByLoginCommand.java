package main.java.com.tsc.apogasiy.tm.command.user;

import main.java.com.tsc.apogasiy.tm.command.AbstractUserCommand;
import main.java.com.tsc.apogasiy.tm.exception.system.AccessDeniedException;
import main.java.com.tsc.apogasiy.tm.util.TerminalUtil;

public class UserUnlockByLoginCommand extends AbstractUserCommand {

    @Override
    public String getCommand() {
        return "user-unlock-by-login";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Unlock user by login";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        final boolean isAdmin = serviceLocator.getAuthService().isAdmin();
        if (!isAuth || !isAdmin)
            throw new AccessDeniedException();
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        final String id = serviceLocator.getUserService().findByLogin(login).getId();
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (id.equals(currentUserId)) throw new AccessDeniedException();
        serviceLocator.getUserService().unlockUserByLogin(login);
    }
}
