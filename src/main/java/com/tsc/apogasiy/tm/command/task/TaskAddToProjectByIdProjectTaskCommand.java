package main.java.com.tsc.apogasiy.tm.command.task;

import main.java.com.tsc.apogasiy.tm.command.AbstractProjectTaskCommand;
import main.java.com.tsc.apogasiy.tm.exception.entity.ProjectNotFoundException;
import main.java.com.tsc.apogasiy.tm.exception.entity.TaskNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.Project;
import main.java.com.tsc.apogasiy.tm.model.Task;
import main.java.com.tsc.apogasiy.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskAddToProjectByIdProjectTaskCommand extends AbstractProjectTaskCommand {

    @Override
    public String getCommand() {
        return "task-add-to-project-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Add task to project by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(userId, projectId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(userId, taskId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        serviceLocator.getProjectTaskService().bindTaskById(userId, projectId, taskId);
    }

}
