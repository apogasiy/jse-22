package main.java.com.tsc.apogasiy.tm.command;

import main.java.com.tsc.apogasiy.tm.enumerated.Role;
import main.java.com.tsc.apogasiy.tm.exception.empty.EmptyNameException;
import main.java.com.tsc.apogasiy.tm.exception.entity.ProjectNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.Project;

import java.util.Optional;

public abstract class AbstractProjectCommand extends AbstractCommand{

    @Override
    public Role[] roles() {
        return Role.values();
    }

    protected void showProject(final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
    }

    protected Project add(final String userId, final String name, final String description) {
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        return new Project(userId, name, description);
    }
}
