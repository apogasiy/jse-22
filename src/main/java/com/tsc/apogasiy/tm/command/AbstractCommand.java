package main.java.com.tsc.apogasiy.tm.command;

import main.java.com.tsc.apogasiy.tm.api.service.IServiceLocator;
import main.java.com.tsc.apogasiy.tm.enumerated.Role;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public Role[] roles() {
        return null;
    }

    public abstract String getCommand();

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract void execute();

    @Override
    public String toString() {
        String result = "";
        String name = getCommand();
        String arg = getArgument();
        String description = getDescription();

        if (name != null && !name.isEmpty()) result += name;
        if (arg != null && !arg.isEmpty()) result += " [" + arg + "]";
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

}
