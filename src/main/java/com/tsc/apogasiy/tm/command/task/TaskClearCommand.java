package main.java.com.tsc.apogasiy.tm.command.task;

import main.java.com.tsc.apogasiy.tm.command.AbstractTaskCommand;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return "task-clear";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Drop all tasks";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        serviceLocator.getTaskService().clear(userId);
    }

}
