package main.java.com.tsc.apogasiy.tm.model;

import main.java.com.tsc.apogasiy.tm.api.entity.IWBS;
import main.java.com.tsc.apogasiy.tm.enumerated.Status;

import java.util.Date;

public class Project extends AbstractOwnerEntity implements IWBS {

    private String name;
    private String description;
    private Status status = Status.NOT_STARTED;
    private Date startDate = null;
    private Date finishDate = null;
    private Date created = new Date();

    public Project(String userId, String name) {
        this.userId = userId;
        this.name = name;
    }

    public Project(String userId, String name, String description) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    @Override
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}
