package main.java.com.tsc.apogasiy.tm.model;

import main.java.com.tsc.apogasiy.tm.api.entity.IWBS;
import main.java.com.tsc.apogasiy.tm.enumerated.Status;

import java.util.Date;

public class Task extends AbstractOwnerEntity implements IWBS {

    private String name;
    private String description;
    private Status status = Status.NOT_STARTED;
    private String projectId = null;
    private Date startDate = null;
    private Date finishDate = null;
    private Date created = new Date();

    public Task(String userId, String name) {
        this.userId = userId;
        this.name = name;
    }

    public Task(String userId, String name, String description) {
        setUserId(userId);
        setName(name);
        setDescription(description);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    @Override
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}
