package main.java.com.tsc.apogasiy.tm.service;

import main.java.com.tsc.apogasiy.tm.api.repository.IOwnerRepository;
import main.java.com.tsc.apogasiy.tm.api.service.IOwnerService;
import main.java.com.tsc.apogasiy.tm.exception.empty.EmptyIdException;
import main.java.com.tsc.apogasiy.tm.exception.empty.EmptyIndexException;
import main.java.com.tsc.apogasiy.tm.exception.entity.EmptyUserIdException;
import main.java.com.tsc.apogasiy.tm.exception.entity.EntityNotFoundException;
import main.java.com.tsc.apogasiy.tm.exception.system.IndexIncorrectException;
import main.java.com.tsc.apogasiy.tm.model.AbstractOwnerEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract  class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E> implements IOwnerService<E> {

    private final IOwnerRepository<E> repository;

    public AbstractOwnerService(IOwnerRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public List<E> findAll(final String userId) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.findAll(userId);
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty()) throw new EmptyUserIdException();
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(userId, comparator);
    }

    @Override
    public E findById(final String userId, final String id) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty()) throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty()) throw new EmptyUserIdException();
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index < 0 || index > repository.getSize(userId)) throw new IndexIncorrectException();
        return repository.findByIndex(userId, index);
    }

    public void clear(final String userId) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty()) throw new EmptyUserIdException();
        repository.clear(userId);
    }

    public void remove(final String userId, final E entity) {
        Optional.ofNullable(entity).orElseThrow(EntityNotFoundException::new);
        repository.remove(userId, entity);
    }

    @Override
    public Integer getSize(final String userId) {
        return repository.getSize(userId);
    }

}
