package main.java.com.tsc.apogasiy.tm.service;

import main.java.com.tsc.apogasiy.tm.api.repository.IProjectRepository;
import main.java.com.tsc.apogasiy.tm.api.service.IProjectService;
import main.java.com.tsc.apogasiy.tm.enumerated.Status;
import main.java.com.tsc.apogasiy.tm.exception.empty.*;
import main.java.com.tsc.apogasiy.tm.exception.entity.EmptyUserIdException;
import main.java.com.tsc.apogasiy.tm.exception.system.IndexIncorrectException;
import main.java.com.tsc.apogasiy.tm.model.Project;

import java.util.Optional;

public class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String userId, final String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        final Project project = new Project(userId, name);
        //project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String userId, final String name, final String description) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        if (description == null || description.isEmpty())
            throw new EmptyDescriptionException();
        final Project project = new Project(userId, name, description);
        //project.setName(name);
        //project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public Project findByName(final String userId, String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        return projectRepository.findByName(userId, name);
    }

    @Override
    public Project updateById(final String userId, String id, String name, String description) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        final Project project = projectRepository.findById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final String userId, Integer index, String name, String description) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index < 0)
            throw new IndexIncorrectException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        final Project project = projectRepository.findByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public boolean existsByIndex(String userId, Integer index) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        return projectRepository.existsByIndex(index);
    }

    @Override
    public boolean existsByName(final String userId, String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        return projectRepository.existsByName(userId, name);
    }

    @Override
    public Project startById(final String userId, String id) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        return projectRepository.startById(userId, id);
    }

    @Override
    public Project startByIndex(final String userId, Integer index) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index < 0)
            throw new IndexIncorrectException();
        return projectRepository.startByIndex(userId, index);
    }

    @Override
    public Project startByName(final String userId, String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        return projectRepository.startByName(userId, name);
    }

    @Override
    public Project finishById(final String userId, String id) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        return projectRepository.finishById(userId, id);
    }

    @Override
    public Project finishByIndex(final String userId, Integer index) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index < 0)
            throw new IndexIncorrectException();
        return projectRepository.finishByIndex(userId, index);
    }

    @Override
    public Project finishByName(final String userId, String name) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        return projectRepository.finishByName(userId, name);
    }

    @Override
    public Project changeStatusById(final String userId, String id, Status status) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(id).isPresent() || id.isEmpty())
            throw new EmptyIdException();
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        return projectRepository.changeStatusById(userId, id, status);
    }

    @Override
    public Project changeStatusByIndex(final String userId, Integer index, Status status) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        Optional.ofNullable(index).orElseThrow(EmptyIndexException::new);
        if (index < 0)
            throw new IndexIncorrectException();
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        return projectRepository.changeStatusByIndex(userId, index, status);
    }

    @Override
    public Project changeStatusByName(final String userId, String name, Status status) {
        if (!Optional.ofNullable(userId).isPresent() || userId.isEmpty())
            throw new EmptyUserIdException();
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        return projectRepository.changeStatusByName(userId, name, status);
    }
}
