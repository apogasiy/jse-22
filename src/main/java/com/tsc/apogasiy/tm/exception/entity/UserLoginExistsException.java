package main.java.com.tsc.apogasiy.tm.exception.entity;

import main.java.com.tsc.apogasiy.tm.exception.AbstractException;

public class UserLoginExistsException extends AbstractException {

    public UserLoginExistsException(final String login) {
        super("Error! User with login '" + login + "' already exists.");
    }

}
