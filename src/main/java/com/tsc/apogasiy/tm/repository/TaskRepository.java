package main.java.com.tsc.apogasiy.tm.repository;

import main.java.com.tsc.apogasiy.tm.api.repository.ITaskRepository;
import main.java.com.tsc.apogasiy.tm.enumerated.Status;
import main.java.com.tsc.apogasiy.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllTaskByProjectId(final String userId, String projectId) {
        return findAll(userId).stream()
                .filter(t -> t.getProjectId().equals(projectId))
                .collect(Collectors.toList());
    }

    @Override
    public Task findByName(final String userId, String name) {
        return findAll(userId).stream()
                .filter(t -> t.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Task removeByName(final String userId, String name) {
        final Task task = findByName(userId, name);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        list.remove(task);
        return task;
    }

    @Override
    public boolean existsByName(final String userId, String name) {
        return findByName(userId, name) != null;
    }

    @Override
    public boolean existsByIndex(final String userId, Integer index) {
        return findByIndex(userId, index) != null;
    }

    @Override
    public boolean existsById(final String userId, String id) {
        return findById(userId, id) != null;
    }

    @Override
    public Task startById(final String userId, String id) {
        final Task task = findById(userId, id);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(final String userId, Integer index) {
        final Task task = findByIndex(userId, index);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(final String userId, String name) {
        final Task task = findByName(userId, name);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(final String userId, String id) {
        final Task task = findById(userId, id);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(final String userId, Integer index) {
        final Task task = findByIndex(userId, index);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(final String userId, String name) {
        final Task task = findByName(userId, name);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeStatusById(final String userId, String id, Status status) {
        final Task task = findById(userId, id);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final String userId, Integer index, Status status) {
        final Task task = findByIndex(userId, index);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(final String userId, String name, Status status) {
        final Task task = findByName(userId, name);
        if (!Optional.ofNullable(task).isPresent())
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task bindTaskToProjectById(final String userId, final String projectId, final String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        task.setUserId(userId);
        return task;
    }

    @Override
    public Task unbindTaskById(final String userId, final String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(null);
        task.setUserId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(final String userId, final String projectId) {
        findAll(userId).stream()
                .filter(t -> projectId.equals(t.getProjectId()))
                .forEach(t -> t.setProjectId(null));
    }
}
